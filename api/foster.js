import request from "@/utils/request.js";

// 获取认养配置页面
export function getAdoptSetUp() {
	return request({
		url: `/jeecg-boot/api/page/getAdoptSetUp`,
		method: 'get',
	})
}

// 获取认养列表
export function getAdoptProject(data) {
	return request({
		url: `/jeecg-boot/api/page/getAdoptProject`,
		method: 'get',
		data
	})
}

// 获取滚动认养列表
export function getAdoptRoll() {
	return request({
		url: `/jeecg-boot/api/page/getAdoptRoll`,
		method: 'get',
	})
}

// 通过id查询详情
export function getProjectById(data) {
	return request({
		url: `/jeecg-boot/api/page/getProjectById`,
		method: 'get',
		data
	})
}

// 认养订单下单
export function createOrder(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptOrder/createOrder`,
		method: 'post',
		data
	})
}

// 计算订单价格
export function computeOrderPrice(data) {
	return request({
		url: `/jeecg-boot/adopt/adoptOrder/computeOrderPrice`,
		method: 'post',
		data
	})
}
